import json
import numpy as np
from flask import Flask, render_template, request, abort, jsonify, Response, make_response
from sklearn.externals import joblib
import pandas as pd
import scipy as sci

app = Flask(__name__)

def response_ok(data):
   response = jsonify({'status': 'success', 'data': data}, 200)
   return make_response(response)


def response_error(message, error=None, error_code=None):
    response = json.dumps({'status': 'fail', 'message': message, 'error': error, 'error_code': error_code})
    return make_response(response, 400)

def check_auth_header():
    username, password = request.authorization.get('username', None), request.authorization.get('password', None)
    if not username or not password:
        return False, "Unable to authenticate user. Set auth header as TEST if you are in test mode."
    if username != "terragonlookalike" or password != "terragonlookalike":
        return False, "Invalid authentication header"
    return True, "Successfully authenticated"

campaign_types = ['BusinessFinance','EventsRelationships','Others','ReligionSpirituality','TechnologyComputing','Sports']

    

@app.route('/lookalike/api/v1.0/', methods=['POST'])
def make_prediction():
    print('I AM IN HERE')
    validate_auth_header = check_auth_header()
    if not validate_auth_header[0]:
       return validate_auth_header[1]
    campaign_type = request.args.get('campaign_type')
    if not campaign_type:
       return response_error("No campaign type passed")
    if campaign_type not in campaign_types:
       return response_error("Invalid Campaign type passed. Campaign types should be one of {}".format(campaign_types))
    

    customers = []
    customer_id = []
    customer_dist_item = dict()
    data = request.files['file']    
    campaign_type = request.args.get('campaign_type')
    pickle_file_name = 'models/{}.pkl'.format(campaign_type)
    model = joblib.load(pickle_file_name)
    
    data = pd.read_csv(data)
    data = data.set_index("msisdn")

    for item in list(data.index):
        customer_id.append(item)
        item = np.array(data.loc[item].values)
        # print(item)
        item = model.fit_transform(item)
        # print(item)
        distance = sci.spatial.distance.euclidean(model.cluster_centers_[0] , item.T)
        customer_dist_item[distance] = item
        # customers.append(item)
    
    # customers = np.vstack(customers)
    # sci.spatial.distance.euclidean(model.cluster_centers_ , customers)
    # prediction = model.predict(customers)
    # print("prediction ok")


    label = dict(zip(customer_id, list(customer_dist_item.keys())))
    for k,v in label.items():
        label[k] = int(v)
    
    return jsonify(label)


if __name__ == '__main__':
    app.run(debug=False)

